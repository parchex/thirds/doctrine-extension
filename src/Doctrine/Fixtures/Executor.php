<?php declare(strict_types=1);

namespace Parchex\Third\Doctrine\Fixtures;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class Executor
{
    /**
     * @var ORMPurger
     */
    private $purger;
    /**
     * @var ORMExecutor
     */
    private $executor;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(
        EntityManagerInterface $manager,
        LoggerInterface $logger = null
    ) {
        $this->manager = $manager;
        $logger = $logger ?? new NullLogger();

        $this->purger = new ORMPurger($manager);

        $this->executor = new ORMExecutor($manager, $this->purger);
        $this->executor->setLogger(
            static function ($message) use ($logger): void {
                $logger->notice($message);
            }
        );
    }

    public function execute(Loader $loader, bool $append = false): void
    {
        $this->executor->execute($loader->getFixtures(), $append);
    }

    public function purge(bool $truncate = false): void
    {
        $this->manager->clear();

        $this->purger->setPurgeMode(
            $truncate ? ORMPurger::PURGE_MODE_TRUNCATE
                : ORMPurger::PURGE_MODE_DELETE
        );
        $this->purger->purge();
    }
}
