<?php

namespace Tests\Parchex\Third\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Parchex\Third\Doctrine\Types\IdentifierType;
use PHPUnit\Framework\TestCase;

/**
 * @testdox Uuid type for doctrine configuration
 * @covers  \Parchex\Third\Doctrine\Types\IdentifierType
 */
class IdentifierTypeTest extends TestCase
{
    /**
     * @test
     */
    public function it_register_specific_uuid_types_whom_name_is_the_uuid_class_name()
    {
        IdentifierType::registerIdentifierType(DummyId::class);
        IdentifierType::registerIdentifierType(OtherId::class);

        /** @var IdentifierType $dummyIdType */
        $dummyIdType = IdentifierType::getType(DummyId::class);
        /** @var IdentifierType $otherIdType */
        $otherIdType = IdentifierType::getType(OtherId::class);

        self::assertTrue(IdentifierType::hasType(DummyId::class));
        self::assertEquals(DummyId::class, $dummyIdType->getIdentifierClass());
        self::assertEquals(DummyId::class, $dummyIdType->getName());
        self::assertTrue(IdentifierType::hasType(OtherId::class));
        self::assertEquals(OtherId::class, $otherIdType->getIdentifierClass());
        self::assertEquals(OtherId::class, $otherIdType->getName());
    }

    /**
     * @test
     */
    public function it_converts_a_uuid_to_database_value()
    {
        $stubPlatform = $this->createMock(AbstractPlatform::class);

        $id = "uno-dos-tres-cuatro";
        $uuid = DummyId::fromString($id);

        /** @var IdentifierType $dcmUuidType */
        $dcmUuidType = $this->createPartialMock(IdentifierType::class, []);

        $value = $dcmUuidType->convertToDatabaseValue($uuid, $stubPlatform);

        self::assertEquals($id, $value);
    }

    /**
     * @test
     */
    public function it_converts_a_uuid_value_from_database_in_a_specific_uuid_object()
    {
        $stubPlatform = $this->createMock(AbstractPlatform::class);

        $dbValue = "uno-dos-tres-cuatro";

        IdentifierType::registerIdentifierType(DummyId::class);

        $uuidType = IdentifierType::getType(DummyId::class);

        $uuid = $uuidType->convertToPHPValue($dbValue, $stubPlatform);

        self::assertInstanceOf(DummyId::class, $uuid);
        self::assertEquals(DummyId::fromString($dbValue), $uuid);
    }
}
