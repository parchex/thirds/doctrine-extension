<?php declare(strict_types=1);

namespace Parchex\Third\Doctrine\Types;

use DateTimeImmutable;
use DateTimeZone;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateTimeTzImmutableType;
use Parchex\Common\DateTime;

class DateTimeType extends DateTimeTzImmutableType
{
    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return $value;
        }

        return parent::convertToDatabaseValue(
            $value
                ->toNative()
                ->setTimezone(new DateTimeZone('UTC')),
            $platform
        );
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value instanceof DateTime) {
            return $value;
        }

        $dateTime = DateTimeImmutable::createFromFormat(
            $platform->getDateTimeTzFormatString(),
            $value,
            new DateTimeZone('UTC')
        );

        if ($dateTime === false) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                $platform->getDateTimeTzFormatString()
            );
        }

        return DateTime::fromString($dateTime->format(DateTime::FORMAT));
    }
}
