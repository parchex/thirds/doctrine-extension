# Changelog

## v0.1.4 - 2021-10-14

- Refactor for clean code
- Adapt new vesion of Makefile
- Upgrade config

## v0.1.2 - 2020-11-23

- **Event storage** with Doctrine
- **Fixtures** loader 
- Different **types** registers
- SQL **logger**
