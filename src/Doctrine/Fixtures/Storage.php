<?php declare(strict_types=1);

namespace Parchex\Third\Doctrine\Fixtures;

use Doctrine\ORM\EntityManagerInterface;
use Parchex\Lump\Fixtures\ObjectBuilder;

class Storage
{
    /**
     * @var FixtureManager
     */
    private $fixtureStorage;

    public function __construct(FixtureManager $fixtureStorage)
    {
        $this->fixtureStorage = $fixtureStorage;
    }

    public static function create(EntityManagerInterface $manager): self
    {
        return new self(new FixtureManager($manager));
    }

    /**
     * @param ObjectBuilder $builder
     * @param array<array<string,string>> $rows
     *
     * @return array<object>
     */
    public function bulk(ObjectBuilder $builder, array $rows): array
    {
        $entities = [];
        foreach ($rows as $fieldValues) {
            $entities[] = $this->createEntityWithAssociations($builder, $fieldValues);
        }

        $this->fixtureStorage->persist($entities);

        return $entities;
    }

    /**
     * @param ObjectBuilder $builder
     * @param array<string,string> $fixtureValues
     * @param bool $checkIfExists
     *
     * @return object
     */
    public function fixture(
        ObjectBuilder $builder,
        array $fixtureValues = [],
        $checkIfExists = false
    ): object {
        $entity = $this->createEntityWithAssociations($builder, $fixtureValues);

        $this->fixtureStorage->persist($entity, $checkIfExists);

        return $entity;
    }

    /**
     * @param ObjectBuilder $builder
     * @param array<string,string> $fixtureValues
     *
     * @return object
     */
    private function createEntityWithAssociations(
        ObjectBuilder $builder,
        array $fixtureValues
    ): object {
        return $builder->fillWith(
            $this->fixtureStorage
                ->generateAssociations(
                    $builder->targetClass(),
                    array_replace($builder->fixtures(), $fixtureValues)
                )
        )->build();
    }
}
