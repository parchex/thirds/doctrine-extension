<?php declare(strict_types=1);

namespace Parchex\Third\Doctrine\Types;

use Acelaya\Doctrine\Type\PhpEnumType;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type;
use Parchex\Common\Enum;
use Parchex\Core\Domain\Identifier;

class TypeRegister
{
    /**
     * @param array<class-string, class-string> $types
     *
     * @throws DBALException
     */
    public static function all(array $types): void
    {
        foreach ($types as $name => $classType) {
            static::add($name, $classType);
        }
    }

    /**
     * @param class-string|object $nameClass
     * @param class-string<Type> $classType
     *
     * @throws DBALException
     */
    public static function add($nameClass, $classType = null): void
    {
        /** @var class-string $classType */
        $classType = $classType ?? $nameClass;
        /** @var class-string $nameClass */
        $nameClass = is_string($nameClass) ? $nameClass : $classType;

        switch (true) {
            case is_subclass_of($nameClass, Enum::class):
                PhpEnumType::registerEnumType($nameClass);
                break;
            case is_subclass_of($nameClass, Identifier::class):
                IdentifierType::registerIdentifierType($nameClass);
                break;
            default:
                Type::addType($nameClass, $classType);
                break;
        }
    }
}
