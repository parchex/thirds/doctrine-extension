<?php declare(strict_types=1);

namespace Parchex\Third\Doctrine\Events;

use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\Persistence\Mapping\ClassMetadata;
use Doctrine\Persistence\Mapping\Driver\MappingDriver as Driver;
use Parchex\Common\DateTime;
use Parchex\Lump\Events\Persistence\Event;

class MappingDriver implements Driver
{
    public const NS_EVENT = Event::class;
    /**
     * @var bool
     */
    private $transient = true;

    /**
     * {@inheritdoc}
     *
     * @throws MappingException
     */
    public function loadMetadataForClass($className, ClassMetadata $metadata)
    {
        $this->transient = false;
        /** ClassMetadataInfo $metadata */
        $metadata->setInheritanceType(ClassMetadataInfo::INHERITANCE_TYPE_NONE);
        $metadata->customRepositoryClassName = DoctrineEventStorage::class;
        $metadata->setPrimaryTable(
            [
                'name' => 'events_storage',
            ]
        );
        $metadata->setChangeTrackingPolicy(
            ClassMetadataInfo::CHANGETRACKING_DEFERRED_IMPLICIT
        );
        $metadata->mapField(
            [
                'fieldName' => 'type',
                'type' => 'string',
                'columnName' => 'type',
                'length' => 150,
            ]
        );
        $metadata->mapField(
            [
                'fieldName' => 'body',
                'type' => 'text',
                'columnName' => 'body',
            ]
        );
        $metadata->mapField(
            [
                'fieldName' => 'occurredOn',
                'type' => DateTime::class,
                'columnName' => 'ocurred_on',
            ]
        );
        $metadata->mapField(
            [
                'id' => true,
                'fieldName' => 'identifier',
                'type' => 'integer',
                'columnName' => 'id',
            ]
        );
        $metadata->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_IDENTITY);
    }

    /**
     * @return array<int, class-string>
     */
    public function getAllClassNames()
    {
        return [Event::class];
    }

    /**
     * @param class-string $className
     *
     * @return bool
     */
    public function isTransient($className)
    {
        return $this->transient;
    }
}
