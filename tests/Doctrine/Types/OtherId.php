<?php declare(strict_types=1);

namespace Tests\Parchex\Third\Doctrine\Types;

use Parchex\Core\Domain\Identifier;

class OtherId extends Identifier
{
}
