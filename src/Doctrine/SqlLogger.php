<?php declare(strict_types=1);

namespace Parchex\Third\Doctrine;

use Psr\Log\LoggerInterface;

/**
 * https://github.com/tuupola/dbal-psr3-logger
 */
class SqlLogger implements \Doctrine\DBAL\Logging\SQLLogger
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var string
     */
    private $sql = "";
    /**
     * @var float
     */
    private $start = null;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $sql
     */
    public function startQuery($sql, array $params = null, array $types = null): void
    {
        $params = $params ?? [];

        $this->start = microtime(true);
        $this->sql = preg_replace_callback(
                "/\?/",
                static function () use (&$params): string {
                    $param = array_shift($params);
                    if ($param === null) {
                        return "NULL";
                    }
                    if (is_array($param)) {
                        return "'" . implode(',', $param) . "'";
                    }

                    return "'" . (string) $param . "'";
                },
                $sql
            ) ?? "";
    }

    public function stopQuery(): void
    {
        $elapsed = (microtime(true) - $this->start) * 1000;
        $this->sql .= " -- {$elapsed}";
        $this->logger->info($this->sql);
    }
}
