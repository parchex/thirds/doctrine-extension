<?php declare(strict_types=1);

namespace Parchex\Third\Doctrine\Types;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Doctrine\DBAL\Types\Type;
use Parchex\Core\Domain\Id\Uuid;
use Parchex\Core\Domain\Identifier;

class IdentifierType extends GuidType
{
    /**
     * @var string
     */
    private $name = Uuid::class;
    /**
     * @var class-string<Identifier>
     */
    private $identifierClass = Uuid::class;

    /**
     * @param class-string<Identifier> $name
     *
     * @throws DBALException
     */
    public static function registerIdentifierType(string $name): void
    {
        Type::hasType($name) ?: Type::addType($name, self::class);

        /** @var IdentifierType $identifierType */
        $identifierType = Type::getType($name);
        $identifierType->addIdentifierType($name);
    }

    /**
     * @param class-string<Identifier> $identifierClass
     */
    public function addIdentifierType(string $identifierClass): void
    {
        $this->identifierClass = $identifierClass;
        $this->name = $identifierClass;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     *
     * @param mixed|null $value
     * @param AbstractPlatform $platform
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) {
            return null;
        }
        if ($value instanceof Identifier) {
            return $value;
        }

        return $this->getIdentifierClass()::fromString($value);
    }

    /**
     * @param Identifier|null $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        return (string) $value;
    }

    /**
     * @return class-string<Identifier>
     */
    public function getIdentifierClass(): string
    {
        return $this->identifierClass;
    }
}
