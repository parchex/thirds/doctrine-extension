<?php

return [
    'settings' => [
        'doctrine' => [
            'dev_mode'   => $_ENV['DEBUG'],
            // path where the compiled metadata info will be cached
            // make sure the path exists and it is writable
            'proxy_dir'  => $_ENV['APP_PATH'] . '/var/cache/proxies',
            'cache_dir'  => $_ENV['APP_PATH'] . '/var/cache/doctrine',
            'connection' => [
                'url' => $_ENV['APP_DB_CONN']
            ]
        ],
    ],
];
