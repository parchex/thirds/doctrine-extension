<?php declare(strict_types=1);

namespace Parchex\Third\Doctrine\Fixtures;

use Closure;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Parchex\Lump\Fixtures\Charger\Value;

class FixtureManager
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var Closure
     */
    private $fncValueAssociation;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;

        $this->fncValueAssociation = function ($value, string $entityClass) {
            // If is an array then is a collection of references of objects persisted
            if (is_array($value)) {
                return array_map(
                    function ($value) use ($entityClass) {
                        return $this->getReference($value, $entityClass);
                    },
                    $value
                );
            }
            // If is null then is null
            if (is_null($value) || $value === '') {
                return null;
            }

            // If are a single value then return reference associated
            return $this->getReference($value, $entityClass);
        };
    }

    /**
     * @param array<int|string,string> $fixtureValues
     * @param string $entityClass
     *
     * @return array<int|string,mixed>
     */
    public function generateAssociations(
        string $entityClass,
        array $fixtureValues
    ): array {
        $fncValueAssociation = $this->fncValueAssociation;

        $metaData = $this->manager->getClassMetadata($entityClass);
        foreach ($metaData->getAssociationMappings() as $field => $prop) {
            $value = Value::set($fixtureValues[$field]);

            if ($metaData->isCollectionValuedAssociation($field)) {
                $value->collection();
            }

            $fixtureValues[$field] = $value->isInstanceOf($prop['targetEntity'])
                ? $fncValueAssociation($value->get(), $prop['targetEntity'])
                : $value->instance($prop['targetEntity'], $this->fncValueAssociation)->get();
        }

        return $fixtureValues;
    }

    /**
     * @param object|array<object> $fixtures
     * @param bool $checkIfExists
     */
    public function persist($fixtures, bool $checkIfExists = false): void
    {
        $fixtures = is_array($fixtures) ? $fixtures : [$fixtures];
        foreach ($fixtures as $fixture) {
            $entityClass = get_class($fixture);
            if (!$checkIfExists ||
                $this->notFound($fixture, $entityClass)
            ) {
                $this->manager->persist($fixture);
            }
        }

        $this->manager->flush();
    }

    /**
     * @param mixed $entity
     * @param class-string $entityClass
     *
     * @return object|null
     *
     * @throws ORMException
     */
    private function getReference($entity, string $entityClass): ?object
    {
        if (is_scalar($entity)) {
            $identifier = (string) $entity;
        } else {
            $identifier = $this->getIdentifier($entity, $entityClass);
            // If are objects have to be persist first
            if ($this->manager->find($entityClass, $identifier) === null) {
                $this->manager->persist($entity);
            }
        }

        return $this->manager->getReference($entityClass, $identifier);
    }

    /**
     * @param object $entity
     * @param class-string $entityClass
     *
     * @return bool
     */
    private function notFound(object $entity, string $entityClass): bool
    {
        $identifier = $this->getIdentifier($entity, $entityClass);

        return $this->manager->find($entityClass, $identifier) === null;
    }

    /**
     * @param object $entity
     * @param class-string $entityClass
     *
     * @return array<string>
     */
    private function getIdentifier(object $entity, string $entityClass): array
    {
        return $this->manager
            ->getClassMetadata($entityClass)
            ->getIdentifierValues($entity);
    }
}
