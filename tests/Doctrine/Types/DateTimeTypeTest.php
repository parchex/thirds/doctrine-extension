<?php

namespace Tests\Parchex\Third\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Parchex\Common\DateTime;
use Parchex\Third\Doctrine\Types\DateTimeType;
use PHPUnit\Framework\TestCase;

/**
 * @testdox DateTime type for doctrine configuration
 * @covers \Parchex\Third\Doctrine\Types\DateTimeType
 */
class DateTimeTypeTest extends TestCase
{
    /**
     * @test
     * @dataProvider dateTimeProvider
     */
    public function it_converts_a_datetime_to_database_value($strDateTime, $databaseExpected)
    {
        $stubPlatform = $this->createMock(AbstractPlatform::class);
        $stubPlatform->method('getDateTimeTzFormatString')
            ->willReturn('Y/m/d H:i:s');

        $dateTime = DateTime::fromString($strDateTime);

        /** @var DateTimeType $dcmDateTimeType */
        $dcmDateTimeType = $this->createPartialMock(DateTimeType::class, []);

        $value = $dcmDateTimeType->convertToDatabaseValue($dateTime, $stubPlatform);

        self::assertEquals($databaseExpected, $value);
    }

    public function dateTimeProvider()
    {
        return [
            ["2012-09-15T12:23:32+00:00", "2012/09/15 12:23:32"],
            ["2015-11-21T23:21:04+02:00", "2015/11/21 21:21:04"]
        ];
    }

    /**
     * @test
     */
    public function it_converts_a_database_value_to_datetime()
    {
        $stubPlatform = $this->createMock(AbstractPlatform::class);
        $stubPlatform->method('getDateTimeTzFormatString')
            ->willReturn('Y/m/d H:i:s');

        $databaseDateTime = "2012/09/15 12:23:32";

        /** @var DateTimeType $dcmDateTimeType */
        $dcmDateTimeType = $this->createPartialMock(DateTimeType::class, []);

        $dateTime = $dcmDateTimeType->convertToPHPValue($databaseDateTime, $stubPlatform);

        self::assertInstanceOf(DateTime::class, $dateTime);
        $expected = DateTime::fromString("2012-09-15T12:23:32+00:00");
        self::assertEquals($expected, $dateTime);
    }
}
