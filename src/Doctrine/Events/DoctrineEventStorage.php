<?php declare(strict_types=1);

namespace Parchex\Third\Doctrine\Events;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Parchex\Lump\Events\Persistence\Event;
use Parchex\Lump\Events\Persistence\EventStorage;

class DoctrineEventStorage extends EntityRepository implements EventStorage
{
    /**
     * @param Event $event
     *
     * @throws ORMException
     */
    public function append(Event $event): void
    {
        $this->getEntityManager()->persist($event);
    }
}
