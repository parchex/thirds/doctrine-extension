<?php

use Psr\Container\ContainerInterface as Container;

/** @var Container $container */
$container['settings']['doctrine'] = array_replace_recursive(
    [
        'dev_mode'   => true,
        'proxy_dir'  => null,
        'mapping'    => [
            __DIR__.'/doctrine',
        ],
        'types'      => [
            \Parchex\Common\DateTime::class => \Parchex\Third\Doctrine\Types\DateTimeType::class,
        ],
        // database configuration parameters
        'connection' => [],
    ],
    (array)$container['settings']['doctrine']
);

// Entity Manager
$container['entity_manager'] = static function (Container $c) {
    return \Doctrine\ORM\EntityManager::create(
        $c->get('settings')['doctrine']['connection'],
        $c->get('doctrine.config')
    );
};

$container['doctrine.config'] = static function (Container $c) {
    $settings = $c->get('doctrine.settings');

    // Register Doctrine types
    \Parchex\Third\Doctrine\Types\TypeRegister::all($settings['types']);

    $config = \Doctrine\ORM\Tools\Setup::createYAMLMetadataConfiguration(
        $settings['mapping'],
        $settings['dev_mode'],
        $settings['proxy_dir'],
        $settings['dev_mode']
            ?
            new \Doctrine\Common\Cache\ArrayCache()
            :
            new \Doctrine\Common\Cache\FilesystemCache($settings['cache_dir'])
    );

    $config->setNamingStrategy(
        new \Doctrine\ORM\Mapping\UnderscoreNamingStrategy(CASE_LOWER)
    );

    $config->setProxyNamespace('Parchex\Proxies');

    $config->setSQLLogger($c->get(\Doctrine\DBAL\Logging\SQLLogger::class));

    return $config;
};

$container[\Doctrine\DBAL\Logging\SQLLogger::class] = function (Container $c) {
    return new \Parchex\Third\Doctrine\SqlLogger($c->get('logger.doctrine'));
};

// Return settings with modules config added
$container['doctrine.settings'] = static function (Container $c) {
    // All settings to review
    return array_reduce(
        // TODO: Apply to other containers
        array_filter($c->get('settings')->all(), 'is_array'),
        // Parse each module settings to searching configuration
        static function ($settings, $cfg) {
            if (isset($cfg['doctrine_mapping'])) {
                $settings['mapping'] = array_merge(
                    $settings['mapping'],
                    $cfg['doctrine_mapping']
                );
            }
            if (isset($cfg['doctrine_types'])) {
                $settings['types'] = array_merge(
                    $settings['types'],
                    $cfg['doctrine_types']
                );
            }

            return $settings;
        },
        // Initial values of settings
        $c->get('settings')['doctrine']
    );
};
